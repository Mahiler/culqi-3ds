const RULES = {
    OFF: "off",
    WARN: "warn",
    ERROR: "error",
};

module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: ["plugin:vue/essential", "airbnb-base", "prettier"],
    parserOptions: {
        ecmaVersion: "latest",
        parser: "@typescript-eslint/parser",
        sourceType: "module",
    },
    plugins: ["vue", "@typescript-eslint"],
    rules: {
        "import/no-unresolved": RULES.OFF,
        "no-underscore-dangle": RULES.OFF,
        "class-methods-use-this": RULES.OFF,
        "vue/no-multiple-template-root": RULES.OFF,
        "vue/multi-word-component-names": RULES.OFF,
        "no-restricted-globals": RULES.OFF,
    },
};
