<p align="center">
  <h1 align="center">Culqi 3DS 🔐</h1>
</p>

Librería para incorporar la autenticación 3DS al flujo de generación de cargo con el fin de minimizar las transacciones fraudulentas por medio de un código o pregunta de seguridad que el cliente tiene que completar para autenticar la transacción.


# **Instalación**

Para inicializar la libreria es necesario importar el script JS minificado.

```html
<script src="https://culqi.com/xxx/culqi-3ds.min.js"></script>
```



# **Configuración**
## **Paso 1: Cargo y tarjeta**

Ingresar la configuración inicial `charge` - `card` como se muestra a continuación.

```javascript
Culqi3DS.settings = {
    charge: {
        totalAmount: 15.99,
        returnUrl: "http://localhost:3000/checkout",
    },
    card: {
        cardNumber: "4456191919191919",
        cvv: "123",
        expirationYear: "2025",
        expirationMonth: "12",
        email: "example@yopmail.com",
    },
};
```

Nota: Esta configuración se tiene que inicializar por cada generación de cargo que se desea realizar.

### Settings

| Parámetro | Tipo            | Por defecto | Descripción                                                     |
| --------- | --------------- | ----------- | --------------------------------------------------------------- |
| charge    | object (Charge) | -           | Permite definir la información del cargo que se está realizando |
| card      | object (Card)   | -           | Permite definir los datos de la tarjeta del usuario             |

### Charge

| Parámetro   | Tipo   | Por defecto | Requerido | Descripción                                                     |
| ----------- | ------ | ----------- | --------- | --------------------------------------------------------------- |
| currency    | string | PEN         | No        | Indica el tipo de moneda del cargo a realizarse (PEN - USD)     |
| totalAmount | number | -           | Si        | Indica el monto total del cargo a realizarse                    |
| returnUrl   | string | -           | Si        | Indica la URL de la página donde se invoca la autenticación 3DS |

### Card

| Parámetro       | Tipo   | Por defecto   | Requerido | Descripción                                          |
| --------------- | ------ | ------------- | --------- | ---------------------------------------------------- |
| cardNumber      | string | -             | Si        | Indica el Nro. de tarjeta                             |
| cvv             | string | -             | Si        | Indica el código de seguridad de la tarjeta          |
| expirationYear  | string | -             | Si        | Indica el año de expiración de la tarjeta            |
| expirationMonth | string | -             | Si        | Indica el mes de expiración de la tarjeta            |
| email           | string | -             | Si        | Indica el correo electrónico del dueño de la tarjeta |
| firstName       | string | autogenerado | No        | Indica los nombres del dueño de la tarjeta           |
| lastName        | string | autogenerado | No        | Indica los apellidos del dueño de la tarjeta         |
| phone           | string | autogenerado | No        | Indica el Nro. de celular del dueño de la tarjeta     |

## **Paso 2: Estilos para modal de error (OPCIONAL)**

Ingresar las opciones de configuración para un modal que se muestra cuando ocurra un error en el flujo de autenticación 3DS.

```javascript
Culqi3DS.options = {
    showModal: true,
    showLoading: true,
    showIcon: true,
    closeModalAction: function(){  /* ... */  },
    style: {
        btnColor: "#3c1361",
        btnTextColor: "#FFFFFF",
    },
};
```

### Options

| Parámetro | Tipo                 | Por defecto | Requerido | Descripción                                                                                   |
| --------- | -------------------- | ----------- | --------- | --------------------------------------------------------------------------------------------- |
| showModal | boolean              | true        | No        | Indica si se va a mostrar un modal por defecto cuando ocurra un error en la autenticación 3DS |
| showIcon | boolean              | true        | No        | Indica si se va a mostrar el icono por defecto dentro del modal de error en la autenticación 3DS |
| showLoading | boolean              | true        | No        | Indica si se va a mostrar el loader por defecto cuando inicie la autenticación 3DS |
| closeModalAction | function              | () => void        | No        | Indica la función que se va a ejecutar cuando se de click al boton aceptar del modal por defecto |
| style     | object (OptionStyle) | -           | No        | Permite personalizar los colores del modal de error para la autenticación 3DS                 |

### OptionStyle

| Parámetro    | Tipo   | Por defecto | Requerido | Descripción                                                     |
| ------------ | ------ | ----------- | --------- | --------------------------------------------------------------- |
| btnColor     | string | #00A19B     | No        | Indica el color del botón para el modal de error (HEX)          |
| btnTextColor | string | #FFFFFF     | No        | Indica el color de texto del botón para el modal de error (HEX) |

## **Paso 3: Llave pública del comercio**

Ingresar la llave pública del comercio del ambiente de integración.

```javascript
Culqi3DS.publicKey = "Aquí inserta tu llave pública";
```

Nota: Esta llave pública permite identificar las comunicaciones entre el cliente (sitio web) y Culqi.

## **Paso 4: Obtención de resultado autenticación 3DS**

Para poder obtener la respuesta del flujo de la autenticación 3DS se necesita implementar un `eventListener` al origin actual que invoca la libreria, como se muestra a continuación:

```javascript
window.addEventListener(
    "message",
    function (event) {
        if (event.origin === window.location.origin) {

            const response = event.data;

            if (response.loading) {
                //Mostrar custom loading
            } else {
                //Ocultar custom loading
            }

            if (response.parameters3DS) {
                // La autenticación 3DS se completó exitosamente
                // response.parameters3DS -> {xid: "0201848b8c23f6b0196cfd", ...}
            } else  if (response.error){
                // (se ha alcanzado el límite de sugerencias) un error en la autenticación 3DS
                // response.error -> “No se pudo autenticar la transacción”
            }
        }
    },
    false
);
```

### Response

| Parámetro     | Tipo                   | Descripción                                                                                                                                                       |
| ------------- | ---------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| parameters3DS | object (Parameters3DS) | Objeto con los parámetros 3DS que da como resultado la autenticación 3DS las cuales sirven para enviar en cuerpo del body para la petición de generación de cargo |
| error         | string        | Texto del error en la autenticación 3DS                                                                                                              |
| loading       | boolean         | Valor que indica cuando se debe mostrar el custom loading o no miesntras se procesa la autentiación 3DS                                                                                                              |

### Parameters3DS

| Parámetro                    | Tipo   | Descripción                                                                       |
| ---------------------------- | ------ | --------------------------------------------------------------------------------- |
| eci                          | string | Identificador unico                                                               |
| xid                          | string | Identificador unico codificado en Base64                                          |
| cavv                         | string | Identificador unico codificado en Base64                                          |
| protocolVersion              | string | Versión de 3DS que se empleó para la autenticación, por ejemplo: 1.0.2 - 2.1.0    |
| directoryServerTransactionId | string | Identificador único de la transacción que solo aparece a partir de la versión 2.x |

# **Uso de la librería**

## **Actual flujo de generación de cargo**

A continuación se muestra un diagrama que resume el flujo actual de generación de cargo sin autenticación 3DS.

<img src="./generate_charge_flow.png"/>

## **Nuevo flujo de generación de cargo**

A continuación se muestra un diagrama que resume el nuevo flujo que se debe seguir para la generación del cargo, añadiendo la autenticación 3DS brindado por esta librería `Culqi 3DS`.

<img src="./culqi_3ds_flow.png"/>
</br> </br>

## **Generación de token Culqi**

Este método tokeniza los datos de la tarjeta para la generación del cargo, la cual para enviar en el objeto del request en la generación del cargo con el key `source_id`.

```javascript
const response = await Culqi3DS.generateToken();
if (response.token) {
    // El token se genero exitosamente
    // response.token -> {id: "tkn_live_xxxxxxxxxxxxxxxx", ...}
} else {
    // Ocurrió un problema en la generación del token
    // response.error -> {user_message: "Mensaje al cliente", ...}
}
```

### Response - generateToken

| Parámetro | Tipo   | Descripción                                                                                                                                      |
| --------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| token     | object | Objeto de la generación del token que se realizó con éxito ( Ver documentación [Culqi Docs - Token](https://apidocs.culqi.com/#/tokens#create) ) |
| error     | object | Objeto del error en la generación del token ( Ver documentación [Culqi Docs - Errores](https://apidocs.culqi.com/#/errores) )                    |

<br/>

## **Generación de deviceFingerPrintId**  
Este método genera un identificador único el cual sirve para enviar en el objeto `antifraud_details` para la generación del cargo con el key `device_finger_print_id`.

```javascript
const devideFingerPrintId = await Culqi3DS.generateDevice();
// '8019959c-fab1-49eb-bbbe-b846d308d8df'
```

<br/>

Ejemplo body request a `https://api.culqi.com/v2/charges` para la generación de cargo añadiendo el `tokenId` y el `deviceId`.

```diff
{
    "amount": 1000,
    "currency_code": "PEN",
    "email": "example@yopmail.com",
+   "source_id": "tkn_live_xxxxxxxxxxxxxxxx",
    "antifraud_details": {
        "first_name": "Fernando",
        "last_name": "Chullo Mamani",
        "phone_number": "956-944-598",
+       "device_finger_print_id": "8019959c-fab1-49eb-bbbe-b846d308d8df"
    }
}
```

Basándose en el HTTP status code del Request se evalúa las acciones a tomar

| StatusCode | Descripción                                                            |
| ---------- | ---------------------------------------------------------------------- |
| 201        | Se generó exitosamente el cargo                                        |
| 200        | La generación de cargo necesita pasar por una autenticación (Culqi3DS) |
| 4xx - 5xx  | Ocurrió un error en la generación del cargo                            |

## **Inicio de autenticación 3DS**

Este método inicia el proceso de autenticación 3DS la cual consiste en que el usuario ingrese el código de autenticación dentro de un formulario, el cual lo despliega la librería. Finalizando este flujo se obtiene algunos parámetros que se tiene que mandar para la generación de cargos, en caso exista algún error aparecerá un mensaje de error (configurable si se quiere que aparezca o no esté modal).

```javascript
// OPCION 1
Culqi3DS.initAuthentication();

// OPCION 2: Pasando el tokenId como parametro al método en caso se haya generado sin usar la libreria
Culqi3DS.initAuthentication("tkn_live_WbWix0bN4fAAZ75z");
```

Ejemplo body request a `https://api.culqi.com/v2/charges` para generación del cargo después de pasar por la autenticación 3DS.

```diff
{
    "amount": 1000,
    "currency_code": "PEN",
    "email": "example@yopmail.com",
    "source_id": "tkn_live_xxxxxxxxxxxxxxxx",
    "antifraud_details": {
        "first_name": "Fernando",
        "last_name": "Chullo Mamani",
        "phone_number": "956-944-598",
        "device_finger_print_id": "8019959c-fab1-49eb-bbbe-b846d308d8df"
    },
+   "authentication_3DS": {
+       "eci":"05",
+       "xid":"02010000755f8c81a4db4c848b8c23f6b0196cfd",
+       "cavv":"63617264696e616c636f6d6d6572636561757468",
+       "protocolVersion":"2.1.0",
+       "directoryServerTransactionId":"755f8c81-a4db-4c84-8b8c-23f6b0196cfd"
+   },
}
```

Nota: Tanto el `device_finger_print_id` como el `source_id` deben ser los mismos valores que se enviaron para la generación del cargo en la primera vez.

## **Reinicio de Sesión**

Como ya se mencionó para algunos pasos se tiene que volver a configurar por cada generación de cargo que se realice, para tal caso es necesario borrar las instancias generadas para un nuevo uso.

```javascript
Culqi3DS.reset();
```

Esto elimina las siguientes instancias:

-   Culqi3DS.settings
-   El objeto `token` generado