/* eslint-disable import/no-extraneous-dependencies */
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import * as path from "path";
import VueTypeImports from "vite-plugin-vue-type-imports";

export default defineConfig({
    plugins: [
        vue(),
        VueTypeImports(),
    ],
    resolve: {
        alias: {
            src: path.resolve(__dirname, "src/"),
        },
    },
    build: {
        rollupOptions: {
            output: {
                assetFileNames: () => `assets/index.modules.css`,
            }
        }
    }
});