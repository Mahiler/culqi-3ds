import { SetupPayerAuthResponse, SetupPayerAuthRequest } from "src/interfaces/cybersourcePayerAuth/setupPayerAuth";
import { EnrollPayerAuthResponse, EnrollPayerAuthRequest } from "src/interfaces/cybersourcePayerAuth/enrollPayerAuth";
import { EnvironmentDFPKeys } from "src/interfaces/scriptDFPNiubiz";
import { ResponseHandler, Service } from "src/interfaces/service";
import { Token, CulqiTokenRequest } from "src/interfaces/culqiToken";

class ServiceImpl implements Service {
    private readonly _culqiApiSecure: string = import.meta.env.VITE_APP_SECURE_API_URL;

    private readonly _culqiApiInternal: string = import.meta.env.VITE_APP_INTERNAL_API_URL;

    private async http<ResType, ReqType>(endPoint: string, payloadBody?: ReqType, headers?: Record<string, string>): Promise<ResponseHandler<ResType>> {
        try {
            const result = await fetch(endPoint, {
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    ...headers,
                },
                body: JSON.stringify(payloadBody),
            });

            const data = await result.json();
            const statusCode = result.status;
            if (result.ok) {
                return { success: true, data, statusCode };
            }
            return { success: false, error: data, statusCode };
        } catch (err) {
            return { success: false, error: "Ocurrio un error", statusCode: 502 };
        }
    }

    public async getEnvironmentNiubiz() {
        return this.http<EnvironmentDFPKeys, unknown>(`${this._culqiApiInternal}/express/public/link/getEnvironmentValues`, {});
    }

    public async sendSetupPayerAuth(payloadBody: SetupPayerAuthRequest) {
        return this.http<SetupPayerAuthResponse, SetupPayerAuthRequest>(`${this._culqiApiSecure}/pci-cybersource-setup`, payloadBody);
    }

    public async sendEnrollPayerAuth(payloadBody: EnrollPayerAuthRequest) {
        return this.http<EnrollPayerAuthResponse, EnrollPayerAuthRequest>(`${this._culqiApiSecure}/pci-cybersource-enrollment`, payloadBody);
    }

    public async getCulqiToken(payloadBody: CulqiTokenRequest, publicKey: string) {
        return this.http<Token, CulqiTokenRequest>(`${this._culqiApiSecure}/v2/tokens`, payloadBody, {
            Authorization: `Bearer ${publicKey}`,
            "X-API-VERSION": "2",
            "X-API-KEY": publicKey,
            "X-CULQI-ENV": "live",
        });
    }
}

export default new ServiceImpl();
