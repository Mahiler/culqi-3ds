/* eslint-disable no-unused-vars */
import { Culqi3DS } from "src/interfaces/culqi3DS";

declare global {
    interface Window {
        tmx_profiling_complete: (sessionId: string) => void;
        localSessionId: string;
        Culqi3DS: Culqi3DS;
    }
}
