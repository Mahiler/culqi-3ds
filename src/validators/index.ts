import Joi from "joi";
import { CurrencyISOCode } from "src/utils/constants";

export const settingsCulqi3DSSchema = Joi.object({
    card: {
        cardNumber: Joi.string().required().error(()=> new Error("El número de tarjeta es requerido")),
        cvv: Joi.string().min(3).max(3).required().error(()=> new Error("El cvv de la tarjeta es requerido")),
        expirationYear: Joi.string().min(4).max(4).required().error(()=> new Error("El año de expiración de la tarjeta es requerido")),
        expirationMonth: Joi.string().min(2).max(2).required().error(()=> new Error("El mes de expiración de la tarjeta es requerido")),
        email: Joi.string()
            .email({ tlds: { allow: false } })
            .required(),
        firstName: Joi.string(),
        lastName: Joi.string(),
        phone: Joi.string(),
    },
    charge: {
        currency: Joi.string().valid(CurrencyISOCode.SOLES, CurrencyISOCode.DOLARES),
        totalAmount: Joi.number().required().error(()=> new Error("El monto del cargo es requerido")),
        returnUrl: Joi.string().uri().required().error(()=> new Error("La url de retorno es requerido")),
    },
});

export const optionsCulqi3DSSchema = Joi.object({
    showModal: Joi.boolean(),
    showLoading: Joi.boolean(),
    showIcon: Joi.boolean(),
    closeModalAction: Joi.function(),
    style: {
        btnColor: Joi.string(),
        btnTextColor: Joi.string(),
    },
});
