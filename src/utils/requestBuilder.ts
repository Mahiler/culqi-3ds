import { CardInformation, Culqi3DSSettings } from "src/interfaces/culqi3DS";
import { SetupPayerAuthRequest } from "src/interfaces/cybersourcePayerAuth/setupPayerAuth";
import { EnrollPayerAuthRequest } from "src/interfaces/cybersourcePayerAuth/enrollPayerAuth";
import { CulqiTokenRequest } from "src/interfaces/culqiToken";

export const generateCulqiToken = (cardInformation: CardInformation): CulqiTokenRequest => ({
    card_number: cardInformation.cardNumber as string,
    cvv: cardInformation.cvv as string,
    expiration_year: cardInformation.expirationYear as string,
    expiration_month: cardInformation.expirationMonth as string,
    email: cardInformation.email as string,
    firstName: cardInformation.firstName,
    lastName: cardInformation.lastName,
    phone: cardInformation.phone,
});
export const generateSetupPayerAuth = (cardInformation: CardInformation, tokenId: string): SetupPayerAuthRequest => ({
    expirationMonth: cardInformation.expirationMonth as string,
    expirationYear: cardInformation.expirationYear as string,
    cardNumber: cardInformation.cardNumber as string,
    merchantName: tokenId,
});

export const generateEnrollPayerAuth = ({ card, charge }: Culqi3DSSettings, tokenId: string, referenceId: string, sessionId: string): EnrollPayerAuthRequest => ({
    currency: charge.currency,
    totalAmount: charge.totalAmount?.toString() as string,
    expirationMonth: card.expirationMonth as string,
    expirationYear: card.expirationYear as string,
    cardNumber: card.cardNumber as string,
    mobilePhone: card.phone,
    returnUrl: `${import.meta.env.VITE_APP_PUBLIC_API_URL}/cybersource_challenge_returned`,
    referenceId,
    merchantName: tokenId,
    firstName: card.firstName,
    lastName: card.lastName,
    email: card.email as string,
    fingerprintSessionId: sessionId,
    deviceInformation: {
        httpBrowserLanguage: navigator.language,
        httpBrowserJavaScriptEnabled: true,
        httpBrowserJavaEnabled: navigator.javaEnabled(),
        httpBrowserColorDepth: screen.colorDepth,
        httpBrowserScreenHeight: window.innerHeight,
        httpBrowserScreenWidth: window.innerWidth,
        httpBrowserTimeDifference: new Date().getTimezoneOffset(),
    },
});
