/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
import { ModalProps } from "src/interfaces/components/modalComponent";

export enum Colors {
    PRIMARY = "#00A19B",
    WHITE = "#FFFFFF",
}

export enum PrefixMerchantPublicKey {
    LIVE = "pk_live_",
    TEST = "pk_test_",
}

export enum CurrencyISOCode {
    SOLES = "PEN",
    DOLARES = "USD",
}

export enum ErrorsMessage {
    PUBLIC_KEY = "Falta configurar la llave publica del comercio",
    TOKEN_ID = "Falta generar o pasar por parametro el TokenId",
    CONFIGURATION_SETTINGS = "Falta configurar el objeto 'Culqi3DS.settings'",
    INIT_AUTHENTICATION_3DS = "Falta configurar el objeto 'settings.card' - 'settings.charge' y/o generar el token de culqi",
}

export const SESSION_EXPIRED = 600; // in seconds

export enum CybersourceAuthenticationResult {
    FAILURE = "6",
}

export enum CybersourceInformationError {
    SYSTEM_ERROR = "SYSTEM_ERROR",
    NOT_FOUND = "Error no encontrado",
    CARD_NOT_FOUND = " Número de tarjeta no identificada",
}

export enum CybersourceVeresEnroll {
    Y = "Y",
    N = "N",
    U = "U",
}
export enum CybersourceParesStatus {
    Y = "Y",
}

export enum CybersourceEci {
    SEVEN = "07",
    ZERO = "00",
}

export enum CybersourceStatus {
    AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED",
    AUTHENTICATION_SUCCESSFUL = "AUTHENTICATION_SUCCESSFUL",
    PENDING_AUTHENTICATION = "PENDING_AUTHENTICATION",
    COMPLETED = "COMPLETED",
    NETWORK = "404-502-400",
}


const TITLE_ERROR_MESSAGE = 'No se pudo procesar el pago';
export const ModalErrorMessages: Record<string, Omit<ModalProps, "primaryColor" | "textColor" | "showIcon">> = {
    CARD_NOT_ENROLLED: {
        title: TITLE_ERROR_MESSAGE,
        description: "La tarjeta no pudo ser identificada por el banco. Intente otro método de pago u otra tarjeta",
    },
    ISSUER_SYSTEM_ERROR: {
        title: TITLE_ERROR_MESSAGE,
        description: "Ocurrió un error al procesar el código de seguridad",
    },
    ISSUER_AUTHENTICATION_FAILED: {
        title: TITLE_ERROR_MESSAGE,
        description: "El banco no pudo autenticar su tarjeta. Intente otro método de pago u otra tarjeta",
    },
    AUTHENTICATION_FAILED: {
        title: TITLE_ERROR_MESSAGE,
        description: "Ha fallado la autenticación. Intente otro método de pago u otra tarjeta",
    },
    SESSION_EXPIRED: {
        title: "Han pasado 10 minutos",
        description: "La sesión expiro, vuelva a intentarlo",
    },
    INVALID_DATA: {
        title: TITLE_ERROR_MESSAGE,
        description: "Verifique los datos ingresados y vuelva a intentarlo",
    },
    ERROR_NOT_FOUND: {
        title: TITLE_ERROR_MESSAGE,
        description: "Ocurrió un error inesperado. Comuníquese con Culqi o vuelva a intentarlo en otro momento",
    },
};
