/* eslint-disable no-console */
// Component based to niubiz script https://s3-gestor-librerias.s3.amazonaws.com/Plugins/dfp_niubiz_prod_pasarela.js
// for passing DeviceFingerPrintId in antifraud process
import { EnvironmentDFPKeys } from "src/interfaces/scriptDFPNiubiz";

const timeout = 3000;
const merchantId = "vndp";
const hostname = "h.online-metrix.net/fp";
const hostname2 = "m.vnforapps.com";
const counterTime = () => Math.floor(Date.now() / 1000);

const doProfile = (orgId: string, sessionId: string, hostName: string) => {
    console.log(`Starting profile using ${hostName} for orgId ${orgId} with sessionId ${sessionId} on ${counterTime()}`);
    const head = document.getElementsByTagName("head").item(0) as HTMLHeadElement;
    const url = `https://${hostName}/tags.js?org_id=${orgId}&session_id=${sessionId}&page_id=1&allow_reprofile=1`;
    console.log(url);
    const script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", url);
    head.appendChild(script);
    console.log("Profile should have started...");
    const body = document.getElementsByTagName("body").item(0) as HTMLHeadElement;
    const noscript = document.createElement("noscript");
    const iframe = document.createElement("iframe");
    iframe.setAttribute("style", "width: 100px height: 100px border: 0 position:absolute top: -5000px");
    iframe.setAttribute("src", url);
    noscript.appendChild(iframe);
    body.insertBefore(noscript, body.childNodes[0]);
};

const startOnTimer = (sessionId: string, csOrgId: string): number => {
    doProfile(csOrgId, merchantId + sessionId, hostname);
    return 0;
};

// start the niubiz script proccess  that receives sessionId => DFP => uuid
function initDFP(sessionId: string, { sasOrgId, csOrgId }: EnvironmentDFPKeys) {
    window.localSessionId = sessionId;
    console.log("Init profiling ", counterTime());
    doProfile(sasOrgId, sessionId, hostname2);
    setTimeout(() => startOnTimer(sessionId, csOrgId), timeout);
}

// finish the niubiz script proccess
window.tmx_profiling_complete = (sessionId) => {
    console.log("Finished ", sessionId, "-", counterTime());
};

export default initDFP;
