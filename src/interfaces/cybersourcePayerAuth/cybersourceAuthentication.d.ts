export interface ConsumerAuthenticationInformation {
    accessToken: string;
    deviceDataCollectionUrl: string;
    referenceId: string;
    stepUpUrl: string;
    veresEnrolled: string;
    paresStatus: string;
    eci?: string;
    eciRaw?: string;
    ucafAuthenticationData?: string;
    xid?: string;
    cavv?: string;
    specificationVersion: string;
    directoryServerTransactionId?: string;
    authenticationResult: string;
}

export interface CybersourceGeneralResponse {
    id: string;
    submitTimeUtc: string;
    status: string;
    clientReferenceInformation: {
        code: string;
    };
    errorInformation?: {
        reason: string;
        message: string;
    };
}
