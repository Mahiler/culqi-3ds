import { ConsumerAuthenticationInformation, CybersourceGeneralResponse } from "src/interfaces/cybersourcePayerAuth/cybersourceAuthentication";

export interface ValidatePayerAuthResponse extends CybersourceGeneralResponse {
    consumerAuthenticationInformation: Omit<ConsumerAuthenticationInformation, "accessToken" | "referenceId" | "deviceDataCollectionUrl" | "stepUpUrl" | "veresEnrolled">;
}
