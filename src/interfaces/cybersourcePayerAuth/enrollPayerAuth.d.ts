import { ConsumerAuthenticationInformation, CybersourceGeneralResponse } from "src/interfaces/cybersourcePayerAuth/cybersourceAuthentication";

export interface DeviceInformation {
    httpBrowserLanguage: string;
    httpBrowserJavaScriptEnabled: boolean;
    httpBrowserJavaEnabled: boolean;
    httpBrowserColorDepth: number;
    httpBrowserScreenHeight: number;
    httpBrowserScreenWidth: number;
    httpBrowserTimeDifference: number;
}

export interface EnrollPayerAuthRequest {
    currency: string;
    totalAmount: string;
    expirationMonth: string;
    expirationYear: string;
    cardNumber: string;
    mobilePhone: string;
    returnUrl: string;
    referenceId: string;
    merchantName: string;
    firstName: string;
    lastName: string;
    email: string;
    fingerprintSessionId: string;
    deviceInformation: DeviceInformation;
}

export interface EnrollPayerAuthResponse extends CybersourceGeneralResponse {
    consumerAuthenticationInformation: Omit<ConsumerAuthenticationInformation, "referenceId" | "deviceDataCollectionUrl">;
}
