import { ConsumerAuthenticationInformation, CybersourceGeneralResponse } from "src/interfaces/cybersourcePayerAuth/cybersourceAuthentication";

export interface SetupPayerAuthRequest {
    expirationMonth: string;
    expirationYear: string;
    cardNumber: string;
    merchantName: string;
}

export interface CybersourceSetupResponse extends CybersourceGeneralResponse {
    consumerAuthenticationInformation: Pick<ConsumerAuthenticationInformation, "referenceId" | "deviceDataCollectionUrl" | "accessToken">;
}

export type SetupPayerAuthResponse = CybersourceSetupResponse | string;
