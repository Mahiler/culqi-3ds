/* eslint-disable no-unused-vars */
import { EnrollPayerAuthRequest, EnrollPayerAuthResponse } from "src/interfaces/cybersourcePayerAuth/enrollPayerAuth";
import { SetupPayerAuthRequest, SetupPayerAuthResponse } from "src/interfaces/cybersourcePayerAuth/setupPayerAuth";
import { EnvironmentDFPKeys } from "src/interfaces/scriptDFPNiubiz";
import { Token, CulqiTokenRequest } from "src/interfaces/culqiToken";

export interface ResponseHandler<T> {
    success: boolean;
    statusCode: number;
    data?: T;
    error?: any;
}

export interface Service {
    getEnvironmentNiubiz: () => Promise<ResponseHandler<EnvironmentDFPKeys>>;
    sendSetupPayerAuth: (payloadBody: SetupPayerAuthRequest) => Promise<ResponseHandler<SetupPayerAuthResponse>>;
    sendEnrollPayerAuth: (payloadBody: EnrollPayerAuthRequest) => Promise<ResponseHandler<EnrollPayerAuthResponse>>;
    getCulqiToken: (payloadBody: CulqiTokenRequest, publicKey: string) => Promise<ResponseHandler<Token>>;
}
