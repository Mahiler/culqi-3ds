import { Token } from "src/interfaces/culqiToken";

export interface ChargeInformation {
    currency: "PEN" | "USD";
    totalAmount: number | null;
    returnUrl: string | null;
}

export interface CardInformation {
    cardNumber: string | null;
    cvv: string | null;
    expirationYear: string | null;
    expirationMonth: string | null;
    email: string | null;
    firstName: string;
    lastName: string;
    phone: string;
}

export interface Culqi3DSSettings {
    card: CardInformation;
    charge: ChargeInformation;
}

export interface Culqi3DSOptions {
    showModal: boolean;
    showLoading: boolean;
    showIcon: boolean;
    closeModalAction: () => void;
    style: {
        btnColor: string;
        btnTextColor: string;
    };
}

export interface Authentication3DSParameters {
    eci: string;
    xid: string;
    cavv: string;
    protocolVersion: string;
    directoryServerTransactionId: string | null;
}

export interface Culqi3DS {
    generateDevice: () => Promise<string | null>;
    generateToken: () => Promise<Token | any>;
    // eslint-disable-next-line no-unused-vars
    initAuthentication: (tokenId?: string) => Promise<void>;
}
