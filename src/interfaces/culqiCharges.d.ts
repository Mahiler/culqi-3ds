import { Token } from "src/interfaces/culqiToken";

export interface GenerateTokenResponse {
    token: null | Token;
    error: null | unknown;
}
