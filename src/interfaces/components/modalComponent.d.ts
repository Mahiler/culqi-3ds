/* eslint-disable no-unused-vars */
export interface ModalProps {
    title: string;
    description: string;
    primaryColor: string;
    textColor: string;
    showIcon: boolean;
}

export interface ModalReference {
    titleModalError: string;
    descriptionModalError: string;
    visibleModalError: boolean;
    visibleModalChallenge: boolean;
    closeModalAction: () => void;
}

export interface ModalEmits {
    (e: "closeModal", visible: boolean): void;
}
