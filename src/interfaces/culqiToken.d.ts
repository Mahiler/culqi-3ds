export interface CulqiTokenRequest {
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    card_number: string;
    cvv: string;
    expiration_year: string;
    expiration_month: string;
}

export interface Token {
    active: boolean;
    card_number: string;
    client: Record<string, unknown>;
    creation_date: number;
    email: string;
    id: string;
    iin: Record<string, unknown>;
    last_four: string;
    metadata: Record<string, unknown>;
    object: string;
    type: string;
}
