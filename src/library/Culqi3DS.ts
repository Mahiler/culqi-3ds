import { CybersourceStatus, CybersourceInformationError, ModalErrorMessages, SESSION_EXPIRED, CybersourceVeresEnroll, CybersourceEci, CybersourceAuthenticationResult, CybersourceParesStatus, Colors, ErrorsMessage } from "src/utils/constants";
import { Culqi3DSOptions, Culqi3DS, Authentication3DSParameters } from "src/interfaces/culqi3DS";
import { ModalProps, ModalReference } from "src/interfaces/components/modalComponent";
import { ValidatePayerAuthResponse } from "src/interfaces/cybersourcePayerAuth/validatePayerAuth";
import { CybersourceSetupResponse } from "src/interfaces/cybersourcePayerAuth/setupPayerAuth";
import { EnrollPayerAuthResponse } from "src/interfaces/cybersourcePayerAuth/enrollPayerAuth";
import { Ref } from "vue";
import Service from "src/services/api";
import * as validators from "src/validators/index";
import * as requestBuilder from "src/utils/requestBuilder";
import CulqiCharges from "src/library/CulqiCharges";

class Culqi3dsImpl extends CulqiCharges implements Culqi3DS {
    private _options: Culqi3DSOptions;

    private _3dsValues = { referenceId: "", challengeResponseReturned: false };

    // eslint-disable-next-line no-undef
    private _interval: NodeJS.Timer | null = null;

    // eslint-disable-next-line no-unused-vars
    constructor(private _modalReference: Ref<ModalReference>, private _modalStyle: Ref<Pick<ModalProps, "primaryColor" | "textColor" | "showIcon">>, private _loadingReference: Ref<boolean>) {
        super();
        this._options = {
            closeModalAction: () => {},
            showModal: true,
            showLoading: true,
            showIcon: true,
            style: { btnColor: Colors.PRIMARY, btnTextColor: Colors.WHITE },
        };
        window.addEventListener("message", this._listenerEvent.bind(this), false);
    }

    set options(options: Culqi3DSOptions) {
        const { error } = validators.optionsCulqi3DSSchema.validate(options);
        if (!error) {
            this._modalStyle.value = {
                primaryColor: options?.style?.btnColor ?? this._options.style.btnColor,
                textColor: options?.style?.btnTextColor ?? this._options.style.btnTextColor,
                showIcon: options?.showIcon ?? this._options.showIcon,
            };
            this._modalReference.value = {...this._modalReference.value, closeModalAction: options?.closeModalAction ?? this._options.closeModalAction};
            this._options = {
                closeModalAction: options?.closeModalAction ?? this._options.closeModalAction,
                showLoading: options?.showLoading ?? this._options.showLoading,
                showModal: options?.showModal ?? this._options.showModal,
                showIcon: options?.showIcon ?? this._options.showIcon,
                style: { ...this._options.style, ...options.style },
            };
        }
    }

    // STEP 1
    public async initAuthentication(tokenId?: string): Promise<void> {
        
        const { error: errorSettings } = validators.settingsCulqi3DSSchema.validate(this._settings);
        
        if (errorSettings) {
            this._postMessageAuthentication(null, errorSettings?.message ?? ErrorsMessage.CONFIGURATION_SETTINGS, false);
            return;
        }

        if(!tokenId && !this._tokenId) {
            this._postMessageAuthentication(null, ErrorsMessage.TOKEN_ID, false);
            return;
        }

        if(tokenId) this._tokenId = tokenId;

        this._postMessageAuthentication(null, null, true);
        const setupAuthenticationRequest = requestBuilder.generateSetupPayerAuth(this._settings.card, this._tokenId as string);
        const { success, data } = await Service.sendSetupPayerAuth(setupAuthenticationRequest);
        if (success) {
            if ((data as CybersourceSetupResponse).status === CybersourceStatus.COMPLETED) {
                this._sendDeviceDataCollectionIFrame(data as CybersourceSetupResponse);
            } else if ((data as string) === CybersourceInformationError.NOT_FOUND) {
                this._openModalError(ModalErrorMessages.ERROR_NOT_FOUND);
            } else {
                this._openModalError(ModalErrorMessages.INVALID_DATA);
            }
        } else {
            this._openModalError(ModalErrorMessages.INVALID_DATA);
        }
        
    }

    // STEP 2
    private _sendDeviceDataCollectionIFrame(data: CybersourceSetupResponse): void {
        (document.getElementById("cardinal_collection_form_input") as HTMLInputElement).value = data.consumerAuthenticationInformation.accessToken;
        (document.getElementById("cardinal_collection_form") as HTMLFormElement).action = data.consumerAuthenticationInformation.deviceDataCollectionUrl;
        this._3dsValues.referenceId = data.consumerAuthenticationInformation.referenceId;
        (document.getElementById("cardinal_collection_form") as HTMLFormElement).submit();
    }

    // STEP 3
    private async _sendEnrollmentAuthentication(sessionId: string): Promise<void> {
        const enrollmentAuthenticationRequest = requestBuilder.generateEnrollPayerAuth(this._settings, this._tokenId as string, this._3dsValues.referenceId, sessionId);
        const { success, data } = await Service.sendEnrollPayerAuth(enrollmentAuthenticationRequest);
        if (success) {
            const resultEnroll = data as EnrollPayerAuthResponse;
            if (resultEnroll.status === CybersourceStatus.AUTHENTICATION_SUCCESSFUL) {
                this._handleCybersrourceStatusSuccessfull(resultEnroll); // No requiere autenticación
            } else if (resultEnroll.status === CybersourceStatus.PENDING_AUTHENTICATION) {
                await this._handlerPendingAuthentication(resultEnroll);
            } else if (resultEnroll.status === CybersourceStatus.AUTHENTICATION_FAILED) {
                this._openModalError(ModalErrorMessages.AUTHENTICATION_FAILED);
            } else {
                this._openModalError(ModalErrorMessages.ISSUER_SYSTEM_ERROR);
            }
        } else {
            this._openModalError(ModalErrorMessages.ERROR_NOT_FOUND);
        }
    }

    private async _handlerPendingAuthentication(resultEnroll: EnrollPayerAuthResponse): Promise<void> {
        this._postMessageAuthentication(null, null, false);
        this._handleModalChallengeVisible(true);
        this._createIframChallenge(resultEnroll);
        let c = 0;
        await new Promise<void>((resolve) => {
            this._interval = setInterval(() => {
                c+=1;
                if (c > SESSION_EXPIRED || this._3dsValues.challengeResponseReturned) {
                    if (!this._3dsValues.challengeResponseReturned) {
                        this._openModalError(ModalErrorMessages.SESSION_EXPIRED);
                        resolve();
                    }
                    this._cleanSessionCybersource();
                }
            }, 1000);
        });
    }

    // STEP 4
    private _createIframChallenge(data: EnrollPayerAuthResponse): void {
        (document.getElementById("step-up-form") as HTMLFormElement).action = data.consumerAuthenticationInformation.stepUpUrl;
        (document.getElementById("cybersource_challenge") as HTMLInputElement).value = data.consumerAuthenticationInformation.accessToken;
        (document.getElementById("cybersource_merchant_data") as HTMLInputElement).value = JSON.stringify({
            token: this._tokenId as string,
            returnUrl: this._settings?.charge.returnUrl,
        });
        (document.getElementById("step-up-form") as HTMLFormElement).submit();
    }

    // STEP 5
    private _evaluateChallengeResponse(data: ValidatePayerAuthResponse): void {
        this._handleModalChallengeVisible(false);
        this._3dsValues.challengeResponseReturned = true;
        if (data.status === CybersourceStatus.AUTHENTICATION_SUCCESSFUL) {
            if (data.consumerAuthenticationInformation?.authenticationResult !== CybersourceAuthenticationResult.FAILURE) {
                this._saveAuthentication3DSParameters(data);
            } else if (data.consumerAuthenticationInformation?.eci === CybersourceEci.SEVEN || data.consumerAuthenticationInformation?.eci === CybersourceEci.ZERO) {
                this._openModalError(ModalErrorMessages.ISSUER_AUTHENTICATION_FAILED);
            }
        } else if (CybersourceStatus.NETWORK.split("-").includes(data.status)) {
            // lambda errors conection
            this._openModalError(ModalErrorMessages.ERROR_NOT_FOUND);
        } else if (data.status === CybersourceStatus.AUTHENTICATION_FAILED) {
            this._openModalError(ModalErrorMessages.AUTHENTICATION_FAILED);
        } else if (data.errorInformation?.reason === CybersourceInformationError.SYSTEM_ERROR) {
            this._openModalError(ModalErrorMessages.ISSUER_SYSTEM_ERROR);
        } else {
            this._openModalError(ModalErrorMessages.ERROR_NOT_FOUND);
        }
    }

    private _cleanSessionCybersource(): void {
        // eslint-disable-next-line no-undef
        clearInterval(this._interval as NodeJS.Timer);
        this._handleModalChallengeVisible(false);
        this._3dsValues.challengeResponseReturned = false;
        (document.getElementById("cardinal_collection_form_input") as HTMLInputElement).value = "";
        (document.getElementById("cardinal_collection_form") as HTMLFormElement).action = "";
        (document.getElementById("step-up-form") as HTMLFormElement).action = "";
        (document.getElementById("cybersource_challenge") as HTMLInputElement).value = "";
        (document.getElementById("cybersource_merchant_data") as HTMLInputElement).value = "";
    }

    private async _listenerEvent(event: any): Promise<void> {
        if (event.origin === import.meta.env.VITE_APP_CARDINAL_API_URL) {
            const deviceData = JSON.parse(event.data);
            await this._sendEnrollmentAuthentication(deviceData.SessionId);
        }

        if (event.origin === import.meta.env.VITE_APP_PUBLIC_API_URL) {
            this._evaluateChallengeResponse(event.data);
        }
    }

    private _handleCybersrourceStatusSuccessfull(data: EnrollPayerAuthResponse): void {
        if (data.consumerAuthenticationInformation?.veresEnrolled === CybersourceVeresEnroll.Y && data.consumerAuthenticationInformation?.paresStatus === CybersourceParesStatus.Y) {
            // version 2.0
            this._saveAuthentication3DSParameters(data);
        } else if (data.consumerAuthenticationInformation?.veresEnrolled === CybersourceVeresEnroll.N) {
            this._openModalError(ModalErrorMessages.CARD_NOT_ENROLLED);
        } else if (data.consumerAuthenticationInformation?.veresEnrolled === CybersourceVeresEnroll.U || data.consumerAuthenticationInformation?.eci === CybersourceEci.SEVEN) {
            this._openModalError(ModalErrorMessages.ISSUER_AUTHENTICATION_FAILED);
        } else {
            this._openModalError(ModalErrorMessages.ISSUER_AUTHENTICATION_FAILED);
        }
    }

    private _saveAuthentication3DSParameters(data: EnrollPayerAuthResponse | ValidatePayerAuthResponse): void {
        const { eci, eciRaw, ucafAuthenticationData, xid, cavv, specificationVersion: protocolVersion, directoryServerTransactionId } = data.consumerAuthenticationInformation;
        const parameters3DS = {
            eci: (eci ?? eciRaw) as string,
            xid: (xid ?? ucafAuthenticationData) as string,
            cavv: (cavv ?? ucafAuthenticationData) as string,
            protocolVersion,
            directoryServerTransactionId: directoryServerTransactionId ?? null,
        };
        this._postMessageAuthentication(parameters3DS, null, false);
    }
    
    private _openModalError(contentModal: Omit<ModalProps, "primaryColor" | "textColor" | "showIcon">): void {
        if (this._options.showModal) {
            this._modalReference.value = {
                ...this._modalReference.value,
                titleModalError: contentModal.title,
                descriptionModalError: contentModal.description,
                visibleModalError: true,
                visibleModalChallenge: this._modalReference.value.visibleModalChallenge,
            };
        }
        this._postMessageAuthentication(null, contentModal.description, false);
    }

    private _handleModalChallengeVisible(visible: boolean) {
        this._modalReference.value.visibleModalChallenge = visible;
    }

    private _postMessageAuthentication (parameters3DS: Authentication3DSParameters |  null, error: string | null, loading: boolean ) {
        if(this._options.showLoading){
            this._loadingReference.value = loading;
        }
        window.postMessage({ parameters3DS, error, loading }, this._settings.charge.returnUrl as string);
    }
}

export default Culqi3dsImpl;
