import { ErrorsMessage, PrefixMerchantPublicKey, CurrencyISOCode } from "src/utils/constants";
import { Culqi3DSSettings } from "src/interfaces/culqi3DS";
import { GenerateTokenResponse } from "src/interfaces/culqiCharges";
import { EnvironmentDFPKeys } from "src/interfaces/scriptDFPNiubiz";
import { uuid } from "vue-uuid";
import { Token } from "src/interfaces/culqiToken";
import { faker } from "@faker-js/faker";
import initDFP from "src/utils/initDFP";
import * as validators from "src/validators/index";
import * as requestBuilder from "src/utils/requestBuilder";
import Service from "src/services/api";

class CulqiCharges {
    protected _settings: Culqi3DSSettings;

    protected _tokenId: string | null = null;

    private _publicKey: string | null = null;

    constructor() {
        this._settings = this._initSettings();
    }

    set publicKey(publicKey: string) {
        if (publicKey.startsWith(PrefixMerchantPublicKey.LIVE) || publicKey.startsWith(PrefixMerchantPublicKey.TEST)) {
            this._publicKey = publicKey;
        }
    }

    set settings(settings: Culqi3DSSettings) {
        const { error } = validators.settingsCulqi3DSSchema.validate(settings);
        if (!error) {
            this._settings = {
                card: { ...this._settings.card, ...settings.card },
                charge: { ...this._settings.charge, ...settings.charge },
            };
        }
    }

    public async generateDevice(): Promise<string | null> {
        const sessionId = uuid.v4();
        const { success, data } = await Service.getEnvironmentNiubiz();
        if (success) {
            const { sasOrgId, csOrgId } = data as EnvironmentDFPKeys;
            initDFP(sessionId, { sasOrgId, csOrgId });
            return sessionId;
        }
        return null;
    }

    public async generateToken(): Promise<GenerateTokenResponse> {
        const { error: errorSettings } = validators.settingsCulqi3DSSchema.validate(this._settings);
        
        if (!this._publicKey) return { token: null, error: ErrorsMessage.PUBLIC_KEY };
        
        if (errorSettings) return { token: null, error: ErrorsMessage.CONFIGURATION_SETTINGS};

        const requestTokenCulqi = requestBuilder.generateCulqiToken(this._settings.card);
        const { success, data, error } = await Service.getCulqiToken(requestTokenCulqi, this._publicKey as string);
        if (success) {
            const token = data as Token;
            this._tokenId = token.id;
            return { token, error: null };
        }
        return { token: null, error };
    }

    public reset(): void {
        this._settings = this._initSettings();
        this._tokenId = null;
    }

    private _initSettings(): Culqi3DSSettings {
        return {
            card: {
                cardNumber: null,
                cvv: null,
                expirationMonth: null,
                expirationYear: null,
                email: null,
                phone: faker.phone.phoneNumber("#########"),
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
            },
            charge: {
                currency: CurrencyISOCode.SOLES,
                returnUrl: null,
                totalAmount: null,
            },
        };
    }
}

export default CulqiCharges;
