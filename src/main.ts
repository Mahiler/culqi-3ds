import { createApp} from "vue";
import App from "src/App.vue";

// INSERT CULQI 3DS SCRIPT
const culqi3DS = document.createElement("div");
culqi3DS.id = "culqi-3ds";
document.body.appendChild(culqi3DS);

// INSERT ANIMATE CSS STYLESHEET
const animeteCSS = document.createElement("link");
animeteCSS.setAttribute("rel", "stylesheet");
animeteCSS.setAttribute("href", "https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css");
document.head.appendChild(animeteCSS);

createApp(App).mount("#culqi-3ds");

declare module "@vue/runtime-core" {
    export interface ComponentCustomProperties {
        $goto: any;
    }
}
