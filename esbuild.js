/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */

const esbuild = require('esbuild');
const glob = require('glob');
const cssModulesPlugin = require('esbuild-css-modules-plugin');

const ASSETS_DIR = "dist/assets";
const LIBRARY_DIR = "dist/library";

esbuild
    .build({
        plugins: [cssModulesPlugin({
            v2: false,
            cssModulesOption: {
                generateScopedName: '[local]'
            },
            inject: (css, digest) => `
                const styleId = 'style_${digest}';
                if (!document.getElementById(styleId)) {
                  const styleElement = document.createElement('style');
                  styleElement.id = styleId;
                  styleElement.textContent = \`${css.replace(/\n/g, '')}\`;
                  document.head.appendChild(styleElement);
                }
            `
        })],
        entryPoints: [`${ASSETS_DIR}/index.modules.css`],
        outfile: `${ASSETS_DIR}/index-css.js`,
        bundle: true,
        minify: false,
    })
    .then(() => {
        console.log("⚡ Culqi 3DS JS in base CSS modules complete! ⚡");
        esbuild
            .build({
                stdin: { contents: '' },
                inject: glob.sync(`${ASSETS_DIR}/*.js`),
                bundle: true,
                minify: true,
                outfile: `${LIBRARY_DIR}/culqi-3ds.min.js`,
            })
            .then(() => console.log("⚡ Culqi 3DS JS build complete! ⚡"))
            .catch(() => process.exit(1));
    }).catch(() => process.exit(1));